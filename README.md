# mHealth client

## Plugin necessari per VSCode

Installare `Live Server` per eseguire il client

## Esecuzione del server di sviluppo

Pulsante destro su `index.html` e cliccare su `Open with Live Server`

## Build

Eseguire `npm run build`. I file verranno compilati e salvati nella folder `dist/`