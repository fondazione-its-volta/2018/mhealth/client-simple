var apiBaseUrl = "http://localhost:3000";
var contactListSelector = "#contact-list";

$( "#form-new-contact").submit(function(event) {
    //prevengo il comportamento di default del form
    event.preventDefault();
    // console.log(event.target);
    //salvo i valori degli input in 3 variabili: firstName, lastName, email
    var firstName = $(event.target).find('input[name="firstName"]').val();
    var lastName = $(event.target).find('input[name="lastName"]').val();
    var email = $(event.target).find('input[name="email"]').val();
    //eseguo la chiamata xhr con la tecnica ajax
    $.ajax({
        type: 'POST',
        url: apiBaseUrl + '/contact', //endpoint
        dataType: 'json',
        data: {
            firstName: firstName,
            lastName: lastName,
            email: email
        }
    })
    .done(function (contacts) {
        console.log("Il server ha restituito",contacts);
        $('#form-new-contact').trigger("reset");
        $('#form-new-contact-message').show();
    })
    .fail(function (err) {
        console.log(err);
        if(err.status == 0) {
            alert("il server è offline");
        }
        else {
            //per semplicità
            alert("mancano i campi obbligatori");
        }
    })
    ;
});


function getContacts() {
    $.ajax({
        type: 'GET',
        url: apiBaseUrl + '/contact',
        dataType: 'json',
        success: function (contacts) {
            if (typeof contacts == "undefined" || contacts == null || contacts.length <= 0) {
                return;
            }
            console.log(contacts);
            $(contactListSelector).html("");
            for (var i = 0; i < contacts.length; i++) {
                var contact = contacts[i];
                
                $(contactListSelector).append(
                    "<div class='card'>" +
                        "<div class='card-body'>" +
                            "<h5 class='card-title'><i class='fa fa-user'></i>&nbsp;" + contact.lastName + " " + contact.firstName +"</h5>" +
                            "<p class='card-text'>" + contact.email + "</p>" +
                        "</div>" +
                    "</div>");
            }
        }
    });
}
