var transitionTime = 500;

$(document).ready(function(){
    //il codice qui sotto viene eseguito una volta che il DOM (Document Object Model) è pronto
    init();
});

//solo per la versione single page
function init(){
    console.log("funzione init");
    //"apro" la pagina home con un'animazione slideDown
    $("[page='home']").show();
    $("[page='home']").slideUp(0).slideDown(transitionTime);
    //chiamo la funzione che inizializza la gestione del cambio di pagina
    initPageSelection();
}

//solo per la versione single page
function initPageSelection() {
    console.log("funzione initPageSelection");
    //associazione dell'evento "click" a tutti gli elementi che hanno l'attributo "page-select"
    $('[page-select]').click(function(event){
        //prevengo il comportamento di default dell'elemento
        event.preventDefault();
        //fermo la propagazione dell'evento ai figli
        event.stopPropagation();
        //prendo il valore dell'attributo "page-select"
        //e lo salvo nella variabile "pageToShow"
        var pageToShow = $(this).attr('page-select');
        //"chiudo" tutte le pagine
        $('[page]').slideUp(transitionTime);
        //"apro" la pagina selezionata
        $("[page='"+pageToShow+"'").slideUp(0).slideDown(transitionTime);
        //chiudo il menu (che in modalità mobile rimarrebbe aperto)
        $('.navbar-collapse').collapse('hide');
    });
}

console.log("app.js caricato correttamente");